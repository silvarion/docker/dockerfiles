# Systemd Dockerfiles

## General Information

**Author:**         Jesús Alejandro Sánchez Dávila

**Maintainers:**     Jesús Alejandro Sánchez Dávila

**Email:**          jsanchez.consultant@gmail.com

## Description

This repository holds dockerfiles that enable systemd on docker containers.

The main purpose is to be able to test Ansible roles in Docker containers using Molecule.

## How to use

The molecule configuration file `molecule.yml` should look as follows:

    platform:
    - name: platform_custom_name
        image: <image_name>:<tag>
        dockerfile: ../common/Dockerfile.j2
        command: /sbin/init
        capabilities:
        - SYS_ADMIN
        volumes:
        - /sys/fs/cgroup:/sys/fs/cgroup:ro
        tmpfs:
        - /run
        - /run/lock
        - /tmp

The Dockerfile template must contain the following Jinja2 lookup code:

    {{ lookup('url', 'https://gitlab.com/silvarion/docker/dockerfiles/-/raw/main/' ~ item.image | replace(":","/") ~ '/Dockerfile', split_lines=False) }}
